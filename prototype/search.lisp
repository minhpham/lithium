;;;; A generic search tool used to search through trees.
(in-package :lithium.search)

(defun tree-search (states goalp successors combiner)
  "Find a state that satisfies the function goalp. Start with states, and
search according to successors and combiners. Note: this function does not
specify a searching strategy."
  (dbg :search ";; Search: ~A~&" states)  ; print debug message

  (cond
    ; haven't found the goal, and no more state left to search: fail
    ((null states) fail)
    ; the first state to be search happens to be the goal: return it
    ((funcall goalp (first states)) (first states))
    ; neither of the above: generate successor states, combine them with
    ; the rest of the states, and search the result
    (t (tree-search
        (funcall combiner
                 (funcall successors (first states))
                 (rest states))
        goalp successors combiner))))

(defun depth-first-search (start goalp successors)
  "Search new states first until goal is reached, or fail."
  (tree-search (list start) goalp successors #'append))

(defun prepend (x y)
  "Prepend x to the start of the list y"
  (append y x))

(defun breadth-first-search (start goalp successors)
  "Search old states first until goal is reached, or fail."
  (tree-search (list start) goalp successors #'prepend))


