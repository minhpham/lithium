;;;; This file implements the model for visitable abstract syntax tree
;;;;
;;;; Note: this kind of implementation is clunky and unnecessary in Common
;;;; Lisp. It is only written this way so that it can be straight-forwardly
;;;; translated to languages without multiple-dispatch (e.g. Java and C++).
;;;;
;;;; TODO: Provide a macro that expands to the same model.

(in-package :lithium.ast)

;;; Define the kinds of abstract syntax nodes
(defclass li.node ()
  ((type :initarg :type :accessor node-type)))

(defclass li.num (li.node)
  ((value :initarg :value :accessor value)))

(defclass li.add (li.node)
  ((lhs :initarg :lhs :accessor lhs)
   (rhs :initarg :rhs :accessor rhs)))

(defclass li.sub (li.node)
  ((lhs :initarg :lhs :accessor lhs)
   (rhs :initarg :rhs :accessor rhs)))

(defclass li.let (li.node)
  ((letargs :initarg :letargs :accessor letargs)
   (letbody :initarg :letbody :accessor letbody)))

(defclass li.lambda (li.node)
  ((lambda-args :initarg :lambda-args :accessor lambda-args)
   (lambda-body :initarg :lambda-body :accessor lambda-body)))

(defclass li.apply (li.node)
  )

(defclass li.id (li.node)
  ((idsymbol :initarg :idsymbol :accessor idsymbol)))

;;; Define generic functions for node and visitor
;; (defgeneric initialize-instance (node)
;;   (:documentation "Auxiliary function for initializing a (abstract syntax) node"))

(defgeneric accept (node visitor)
  (:documentation "Accept a visitor into the (abstract syntax) node."))

(defgeneric visit (visitor node)
  (:documentation "Visit a (abstract syntax) node"))

;;; Define the specification of accept generic function
(defmethod accept ((node li.node) visitor)
  (visit visitor node))

;;; Define the parser
(defun parse (sexp)
  (cond
    ((numberp sexp) (make-instance 'li.num
                                   :type 'li.num
                                   :value sexp))
    ((symbolp sexp) (make-instance 'li.id
                                   :type 'li.id
                                   :idsymbol sexp))
    ((listp sexp)
     (case (first sexp)
       (+ (make-instance 'li.add
                          :type 'li.add
                          :lhs (parse (second sexp))
                          :rhs (parse (third sexp))))
       (- (make-instance 'li.sub
                          :type 'li.sub
                          :lhs (parse (second sexp))
                          :rhs (parse (third sexp))))
       ('let (make-instance 'li.let
                           :type 'li.let
                           :letargs (second sexp)
                           :letbody (parse (third sexp))))
       ('lambda (make-instance 'li.lambda
                 :type 'li.lambda
                 :lambda-args (second sexp)
                 :lambda-body (parse (third sexp))))
       ))))

;;; Define the kinds of visitors
(defclass visitor () ())
(defclass ast-print-visitor (visitor) ())
(defclass interpret-visitor (visitor) ())

;;; Define the specification for the generic visit method
(defmethod visit ((visitor ast-print-visitor) node)
  (case (node-type node)
    (li.num (format nil "(num ~a)" (value node)))
    (li.add (format nil "(add ~a ~a)"
                    (visit visitor (lhs node))
                    (visit visitor (rhs node))))
    (li.sub (format nil "(sub ~a ~a)"
                    (visit visitor (lhs node))
                    (visit visitor (rhs node))))
    (li.let (format nil "(let ~a ~a)"
                    (letargs node)
                    (visit visitor (letbody node))))
    (li.id (format nil "(id ~a)" (idsymbol node)))))

;;; Lookup name in bindings (probably deffered substitution
;;; environment)
(defun lookup (name bindings))

;;; Visit the nodes, carrying a deferred substitution environment
;;; with it
(defmethod visit ((visitor interpret-visitor) node dfs)
  (case (node-type node)
    (li.num (value node))
    (li.add (+ (visit visitor (lhs node) dfs)
               (visit visitor (rhs node) dfs)))
    (li.sub (- (visit visitor (lhs node) dfs)
               (visit visitor (rhs node) dfs)))
    (li.let ())
    (li.lambda ())
    (li.id (lookup (idsymbol node) dfs))))
