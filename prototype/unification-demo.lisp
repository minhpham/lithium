(load "lithium")

(in-package :lithium.unification)

(defmacro demo (sexpr)
  `(format t "~A => ~A~&" ',sexpr ,sexpr))

(demo (unify '(?x + 1) '(2 + ?y)))  ; => ((?Y . 1) (?X . 2))
(demo (unify '?x '?y))              ; => ((?X . ?Y))
(demo (unify '(?x ?x) '(?y ?y)))    ; => ((?Y . ?Y) (?X . ?Y))

(demo (unify '(?x ?x ?x) '(?y ?y ?y))) ; => ((?X . ?Y))

(demo (unify '(?x ?y) '(?x ?y)))    ; => ((T . T))

(demo (unify '(?x ?x) '(?y ?y)))    ; => ((?X . ?Y))
(demo (unify '(?y ?x) '(?x ?y)))    ; => ((?Y . ?X))

(demo (unify '(?x ?y a) '(?y ?x ?x)))   ; => ((?Y . A) (?X . ?Y))

(demo (unify '?x '(f ?x)))          ; => ((?X . (F ?X)))

(demo (unify '(?x ?y ?z) '((?y ?z) (?z ?x) (?y ?x))))

(trace subst-bindings)

(demo (unifier '(?x ?y a) '(?y ?x ?x)))
(demo (unifier '((?a * ?x ^ 2) + (?b * ?x) + ?c)
               '(?z + (4 * 5) + 3)))