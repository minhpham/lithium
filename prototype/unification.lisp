(in-package :lithium.unification)

;;; Unify VAR with X, using (possibly extending) bindings.
(defun unify-variable (var x bindings)
  (cond
    ((get-binding var bindings)
     (unify (lookup var bindings) x bindings))
    ((and (variable-p x) (get-binding x bindings))
     (unify var (lookup x bindings) bindings))
    (t (extend-bindings var x bindings))))

;;; See if X and Y match with given BINDINGS, then bind if there
;;; is a match, and return bindings.
(defun unify (x y &optional (bindings no-bindings))
  (cond
    ((eq bindings fail) fail)
    ((eql x y) bindings)
    ((variable-p x) (unify-variable x y bindings))
    ((variable-p y) (unify-variable y x bindings))
    ((and (consp x) (consp y))
     (unify (rest x) (rest y)
            (unify (first x) (first y) bindings)))
    (t fail)))

;;; Substitute the value of variables in bindings into x, taking
;;; recurstively bound variables into account
(defun subst-bindings (bindings x)
  (cond
    ((eq bindings fail) fail)
    ((eq bindings no-bindings) x)
    ((and (variable-p x) (get-binding x bindings))
     (subst-bindings bindings (lookup x bindings)))
    ((atom x) x)
    (t (cons (subst-bindings bindings (car x))
             (subst-bindings bindings (cdr x))))))

;;; Return the result of unifying X with Y
(defun unifier (x y)
  (subst-bindings (unify x y) x))
