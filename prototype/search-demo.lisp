(load "search")
(load "debug")

(defpackage :lithium.search-demo
  (:use :cl :cl-user :lithium.debug :lithium.search))

(in-package :lithium.search-demo)

(dbg+ :search)

;; A toy function to generate an infinite tree
(defun binary-tree (x) (list (* 2 x) (+ 1 (* 2 x))))

;; A toy function that returns a function that does
;; an equal comparison
(defun is (value) #'(lambda (x) (eql x value)))

;(depth-first-search 1 (is 12) #'binary-tree)  ; will not stop

(breadth-first-search 1 (is 12) #'binary-tree)

