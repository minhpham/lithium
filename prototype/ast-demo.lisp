(load "lithium")

(in-package :lithium.ast)

(defmacro demo (sexpr)
  `(format t "~A => ~A~&" ',sexpr ,sexpr))

;;; Test cases for the parser
(format t "====================================================~%")
(demo (parse 3))
(demo (parse '(+ 2 3)))
(demo (parse '(- 2 3)))
(demo (parse '(+ (- 3 4) 7)))
(demo (parse '(let ((x 5)) (+ x x))))

;;; Test cases for visitor
(format t "====================================================~%")
(defvar printer (make-instance 'ast-print-visitor))
(demo (visit printer (parse '3)))
(demo (visit printer (parse '(+ 2 3))))
(demo (visit printer (parse '(- 2 3))))
(demo (visit printer (parse '(+ 1 2 3))))
(demo (visit printer (parse '(+ (- 3 4) 7))))
(demo (visit printer (parse '(let ((x 5)) (+ x x)))))

(defvar interpreter (make-instance 'interpret-visitor))
(format t "====================================================~%")
(demo (visit interpreter (parse '3)))
(demo (visit interpreter (parse '(+ 2 3))))
(demo (visit interpreter (parse '(- 2 3))))
(demo (visit interpreter (parse '(+ 1 2 3))))
(demo (visit interpreter (parse '(+ (- 3 4) 7))))
(demo (visit interpreter (parse '(let ((x 5)) (+ x x)))))