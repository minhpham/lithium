(in-package :lithium.common)

(defconstant fail nil "Indicates pattern-match failure")

(defconstant no-bindings '((t. t))
  "Indicates pattern-match success, with no variables")

(defun reuse-cons (x y x+y &key (test #'eql))
  (if (and (funcall test x (car x+y))
           (funcall test y (cdr x+y)))
      x+y
      (cons x y)))
