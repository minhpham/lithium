;;;; Define utility functions and macros that would be useful for
;;;; building the rest of the language
(defpackage :lithium.util
  (:use :cl :cl-user)
  (:export :flatten
           :mkstr
           :symb
           :defmacro!))

(in-package :lithium.util)

;;; Flatten a tree structure into a list containing only the leaves.
(defun flatten (tree)
  (labels ((rec (tree acc)
             (cond ((null tree) acc)
                   ((atom tree) (cons tree acc))
                   (t (rec (car tree)
                           (rec (cdr tree) acc))))))
    (rec tree nil)))

;;; Combine the print value of its arguments into a string.
;;; Like string concatentation, but work on arbitrary types, not
;;; just strings
(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

;;; Make a symbol from an arbitrary string
(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))

;;; Return true if the symbol S is a G-bang symbol.
;;; A G-bang symbol, when used by defmacro/g!, is automatically
;;; wrapped in a gensym, which means that its name is guaranteed
;;; not to collide with any other variable.
(defun g!-symbol-p (s)
  (and (symbolp s)
       (> (length (symbol-name s)) 2)
       (string= (symbol-name s) "G!"
                :start1 0 :end1 2)))

;;; Automatically G-bang symbols with gensym. Subsequent reference
;;; to the same variable should be prefixed with g-bang as well.
;;; Otherwise behave the same as defmacro
(defmacro defmacro/g! (name args &rest body)
  (let ((syms (remove-duplicates
               (remove-if-not #'g!-symbol-p (flatten body)))))
    `(defmacro ,name ,args
       (let ,(mapcar
              (lambda (s)
                `(,s (gensym ,(subseq (symbol-name s) 2))))
              syms)
         ,@body))))

;;; Return true if the symbol s is a O-bang symbol.
;;; Whenever an O-bang symbol is seen by defmacro!, it is evaluated
;;; once at the point where it is seen.
(defun o!-symbol-p (s)
  (and (symbolp s)
       (> (length (symbol-name s)) 2)
       (string= (symbol-name s) "O!"
                :start1 0 :end1 2)))

;;; Generate the G-bang equivalence of a O-bang symbol. The G-bang
;;; symbol can then be used to refer to expression associated with
;;; the O-bang symbol, without evaluating it once more.
;;; NOTE: doesn't make sense to be used on non-O-bang symbols
(defun o!-symbol-to-g!-symbol (s)
  (symb "G!" (subseq (symbol-name s) 2)))

;;; Automatically gensym any G-bang symbol encountered. Also
;;; automatically evaluate any O-bang symbol encountered once.
;;; Otherwise behave the same as defmacro
(defmacro defmacro! (name args &rest body)
  (let* ((os (remove-if-not #'o!-symbol-p args))
         (gs (mapcar #'o!-symbol-to-g!-symbol os)))
    `(defmacro/g! ,name ,args
       `(let ,(mapcar #'list (list ,@gs) (list ,@os))
          ,(progn ,@body)))))

;; Quick tests
;; (flatten '((cond ((null tree) acc)
;;                  ((atom tree) (cons tree acc))
;;                  (t (rec (car tree)
;;                          (rec (cdr tree) acc))))))

;; (mkstr 'blah "lol " 123 '(5 6 7))

;; (g!-symbol-p 'G!anyname)
;; (g!-symbol-p 'g!anyname)
;; (g!-symbol-p 'anyname)

;; (o!-symbol-p 'O!foo)
;; (o!-symbol-p 'o!bar)
;; (o!-symbol-p 'baz)

;; (o!-symbol-to-g!-symbol 'O!foo)
;; (o!-symbol-to-g!-symbol 'o!bar)
;; (o!-symbol-to-g!-symbol 'baz)









