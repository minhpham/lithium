;;;; Implements a prolog interpreter
;;;;
;;;; Clauses are defined implemented as lists of the form
;;;; (head . body) with the head being the fact and body being the
;;;; condition.
;;;; The first element in the head of the clause is the predicate.
;;;; Predicates are used as the index of the clause in a database.
;;;;
;;;; Proofs are done by proving the goals in the body of the clause.
;;;; Facts have no body so they will be immediately "proven."

(in-package :lithium.prolog)

;;; The database holding prodicates
(defvar *db-predicates* nil)

(defun clause-head (clause) (first clause))  ;;; Get the head of a clause
(defun clause-body (clause) (rest clause))   ;;; Get the body of a clause

;;; Get all clauses in a predicate
(defun get-clauses (pred) (get pred 'clauses))

;;; Get a predicate
(defun predicate (relation) (first relation))

;;; Add a clause to the database, indexed by the head's predicate
(defun add-clause (clause)
  (let ((pred (predicate (clause-head clause))))
    ;; The predicate must be non-variable symbol
    (assert (and (symbolp pred) (not (variable-p pred))))
    (pushnew pred *db-predicates*)
    (setf (get pred 'clauses)
          (nconc (get-clauses pred) (list clause)))
    pred))

;;; UI Macro to add a clause to the database
(defmacro <- (&rest clause)
  `(add-clause ',clause))

;;; Remove all clauses for a single predicate
(defun clear-predicate (predicate)
  (setf (get predicate 'clauses) nil))

;;; Remove all clauses for all predicates in the database.
(defun clear-db ()
  (mapc #'clear-predicate *db-predicates*))

;;; Return a list of possible solutions to GOAL.
;; (defun prove (goal bindings)
;;   (mapcan #'(lambda (clause)
;;               (let))))
