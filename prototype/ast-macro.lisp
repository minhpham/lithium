;;;; Define macros to define abstract syntax tree and parser

(defpackage :lithium.ast-macro
  (:use :cl :cl-user :lithium.util))

(in-package :lithium.ast-macro)

(defmacro defast (node-specs)
   `(labels ((,specs ,(mapcar #'car) ,@node-specs))
      (defclass ,name (node)
        ,@args)

      (defmethod initialize-instance :after ((node ,name) &key)
       (setf (slot-value node 'name)
             (format nil "(~a ~a)" ',name
                     (princ-to-string (slot-value node 'value)))))

      (defun parse (sexp)
        (cond
          ((numberp sexp) (make-instance 'num :type 'number :value sexp))
          ((listp sexp)
           (case (first sexp)
             ('+ (make-instance 'add
                                :type 'function
                                :lhs (parse (second sexp))
                                :rhs (parse (third sexp))))
             ('- (make-instance 'sub
                                :type 'function
                                :lhs (parse (second sexp))
                                :rhs (parse (third sexp))))))))))

;;; Bind variables names in n with their definitions in letargs
(defmacro nlet (n letargs &rest body)
  `(labels ((,n ,(mapcar #'car letargs)
              ,@body))
     (,n ,@(mapcar #'cadr letargs))))

(defmacro def-abstract (ast)
  (let ((names (mapcar #'car node-specs))
        (specs (mapcar #'cdr node-specs)))
    (dolist (name names)
      `(defclass ,name))))

(defmacro! def-node (node-spec super)
  (let ((g!name (first node-spec))
        (g!attr (second node-spec))
        (g!super super))
    `(defclass ,g!name (,g!super)
       ,(mapcar (lambda (attr)
                  `(,(symb "NODE-" attr)
                     :initarg :node-type
                     :accessor ,(symb "NODE-" attr)))
                g!attr))))

(macroexpand '(def-node (node () ()) nil))
(macroexpand '(def-node (num (value) ()) node))
(macroexpand '(def-node (add (lhs rhs) ()) node))

(macroexpand-1 '(def-node (node () ()) nil))
(macroexpand-1 '(def-node (num (n) ()) node))

(dolist (num nil)
  (print (* num num)))

;; (node ()) => (defclass node()
;;                ((node-type :initarg :node-type :accessor node-type)))

(node ()
      ((num (n numberp))
       (add (lhs numberp) (rhs numberp))))

(node ()
      ((num ((n numberp)))
       ())
 )

(macroexpand-1 '(def-abstract ((num (n numberp))
                               (add (lhs nodep) (rhs nodep)))))

