;;;; Central package file for Project Lithium

;;; Define all packages
(in-package :cl-user)

(defpackage lithium.common
  (:use :cl :cl-user)
  (:export :fail
           :no-bindings
           :reuse-cons))

(defpackage :lithium.debug
  (:use :cl :cl-user)
  (:export :dbg
           :dbg+
           :dbg-))

(defpackage :lithium.search
  (:use :cl :cl-user :lithium.debug :lithium.common)
  (:export :tree-search
           :depth-first-search
           :breadth-first-search))

(defpackage :lithium.pat-match
  (:use :cl :cl-user :lithium.common)
  (:export :variable-p
           :get-binding
           :binding-val
           :lookup
           :extend-bindings
           :match-variable))

(defpackage :lithium.unification
  (:use :cl :cl-user :lithium.common :lithium.pat-match)
  (:export :unify
           :unify-variable
           :unifier))

(defpackage :lithium.prolog
  (:use :cl :cl-user :lithium.unification :lithium.pat-match)
  (:export :<-
           :predicate))

(defpackage :lithium.ast
  (:use :cl :cl-user)
  (:export :li.node
           :parse
           :visit
           :ast-print-visitor
           :interpret-visitor))

(defpackage :lithium
  (:use :cl :cl-user
        :lithium.common
        :lithium.debug
        :lithium.search
        :lithium.pat-match
        :lithium.prolog
        :lithium.ast))
