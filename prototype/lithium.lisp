;;;; Central facade file for Project Lithium
;;;; Load this file to access public functions

;;; Load all files (after package definition)
(load "lithium-packages")
(load "common")
(load "debug")
(load "search")
(load "pat-match")
(load "unification")
(load "prolog")
(load "ast")
