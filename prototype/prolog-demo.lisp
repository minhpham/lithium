(load "lithium")

(in-package :lithium.prolog)

(trace predicate)

(<- (likes Kim Robin))   ; Kim likes Robin
(<- (likes Sandy Lee))   ; Sandy likes Lee
(<- (likes Sandy Kim))   ; Sandy likes Kim
(<- (likes Robin cats))  ; Robin likes cat

(<- (likes Sandy ?x) (likes ?x cats))   ; Sandy likes anyone who likes cats

; Kim likes anyone who likes both Lee and Kim
(<- (likes Kim ?x) (likes ?x Lee) (likes ?x Kim))