.PHONY: all clean

all: clean
	javacc Parser.jj
	javac *.java

check: all
	reset
	cat test | java Parser
	@echo "Testing Addition ..."
	-echo "(+ 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Subtraction ..."
	-echo "(- 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Multiplication ..."
	-echo "(* 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Division ..."
	-echo "(/ 120 6)" | java Parser
	@echo "===================================================================="
	@echo "Testing Nested Arithmetic Expression ..."
	-echo "(* 6 (+ 5 2 12) 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Complex Arithmetic Expression ..."
	-echo "(* 4 (+ 5 (- 7 2) (/ 12 6) 4 4 (* 22 3)) (- 9 6))" | java Parser
	@echo "===================================================================="
	@echo "Testing Car ..."
	-echo "(car (1 2 3 4 5))" | java Parser
	-echo "((car (1 2)) (car (3 4)) (car (4 5)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Cdr ..."
	-echo "(cdr (1 2 3 4 5))" | java Parser
	-echo "((cdr (1 2)) (cdr (3 4)) (cdr (4 5)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Cons ..."
	-echo "(cons 1 (2 3 4 5))" | java Parser
	@echo "===================================================================="
	@echo "Testing Nested List Operations ..."
	-echo "(car (1 2 3 (car (4 5 6)) 5 6))" | java Parser
	@echo "===================================================================="
	@echo "Testing Complex List Operations ..."
	-echo "(car (cdr (cons 1 (2 3 4 (cons 5 ()) (cdr (1 7 8 9))))))" | java Parser
	@echo "===================================================================="
	@echo "Testing Lambdas & Funcalls ..."
	-echo "(funcall (lambda (x) (+ x x)) 5)" | java Parser
	-echo "((lambda (H) ((lambda (f) ((lambda (d/dx) (d/dx 10)) (lambda (x) (/ (- (f (+ x H)) (f x) H))))) (lambda (x) (* x x x x)))) .001)" | java Parser
	@echo "===================================================================="
	@echo "Testing Let var ..."
	-echo "(let ((x 5)) (let ((double (lambda (x) (+ x x)))) (funcall double 10)))" | java Parser
	-echo "(let ((x 3)) (let ((f (lambda (y) (+ x y)))) (let  ((x 5)) (funcall f 4))))" | java Parser
	-echo "(let ((x (+ 5 5))) (+ x x))" | java Parser
	-echo "(let ((x 5))(+ x x))" | java Parser
	-echo "(let ((x (+ 5 5))) (let ((y (- x 3))) (+ y y)))" | java Parser
	-echo "(let ((x 5)) (let ((y (- x 3))) (+ y y)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((x 3)) 10)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((x 3)) x)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((y 3)) x)))" | java Parser
	-echo "(let ((x 5)) (let ((y x)) y))" | java Parser
	-echo "(let ((x 5)) (let ((x x)) x))" | java Parser
	-echo "(let ((H .001)) (let ((f (lambda (x) (* x x x x)))) (let ((d/dx (lambda (x) (/ (- (funcall f (+ x H)) (funcall f x)) H)))) (funcall d/dx 10))))" | java Parser
	@echo "===================================================================="
	@echo "Testing Let multi-var bindings ..."
	-echo "(let ((x 5) (y 10)) (+ x y))" | java Parser
	-echo "(let ((x 5) (y 10) (z 15)) (+ x y z))" | java Parser
	-echo "(let ((x 5) (y 10)) (let ((f (lambda (g) (* g (/ y x))))) (funcall f (+ x y))))" | java Parser
	@echo "===================================================================="
	@echo "Testing LetRec (foldr) ..."
	-echo "(letrec ((f (lambda (f1 v l) (if l (funcall f1 (car l) (funcall f f1 v (cdr l))) v)))) (funcall f cons () (1 2 3 4 5 6)))" | java Parser
	@echo "===================================================================="
	@echo "Testing LetRec (foldl) ..."
	-echo "(letrec ((f (lambda (f1 v l) (if l (funcall f f1 (funcall f1 (car l) v) (cdr l)) v)))) (funcall f cons () (1 2 3 4 5 6)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Multiple Parameters ..."
	-echo "(let ((sum (lambda (x y) (+ x y)))) (funcall sum 5 10))" | java Parser
	@echo "===================================================================="
	@echo "Testing List Combinators ..."
	@echo "===================================================================="
	@echo "Testing Whole Number Combinators ..."
	-echo "(<i> 10)" | java Parser
	-echo "(<k> 10 20)" | java Parser
	-echo "(<s> (lambda (x y) (+ x y)) (lambda (y) (* y y)) 10)" | java Parser
	-echo "(<c> (lambda (x y) (- x y)) 10 20)" | java Parser
	-echo "(<b> (lambda (x) (* x x)) (lambda (x) (+ x x)) 10)" | java Parser
	@echo "===================================================================="


checkae : all
	reset
	@echo "Testing Addition ..."
	-echo "(+ 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Subtraction ..."
	-echo "(- 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Multiplication ..."
	-echo "(* 6 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Division ..."
	-echo "(/ 120 6)" | java Parser
	@echo "===================================================================="
	@echo "Testing Nested Arithmetic Expression ..."
	-echo "(* 6 (+ 5 2 12) 7)" | java Parser
	@echo "===================================================================="
	@echo "Testing Complex Arithmetic Expression ..."
	-echo "(* 4 (+ 5 (- 7 2) (/ 12 6) 4 4 (* 22 3)) (- 9 6))" | java Parser
	@echo "===================================================================="

checklist : all
	reset
	@echo "Testing Car ..."
	-echo "(car (1 2 3 4 5))" | java Parser
	-echo "((car (1 2)) (car (3 4)) (car (4 5)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Cdr ..."
	-echo "(cdr (1 2 3 4 5))" | java Parser
	-echo "((cdr (1 2)) (cdr (3 4)) (cdr (4 5)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Cons ..."
	-echo "(cons 1 (2 3 4 5))" | java Parser
	@echo "===================================================================="
	@echo "Testing Nested List Operations ..."
	-echo "(car (1 2 3 (car (4 5 6)) 5 6))" | java Parser
	@echo "===================================================================="
	@echo "Testing Complex List Operations ..."
	-echo "(car (cdr (cons 1 (2 3 4 (cons 5 ()) (cdr (1 7 8 9))))))" | java Parser
	@echo "===================================================================="

checkfun : all
	@echo "Testing Lambdas & Funcalls ..."
	-echo "(funcall (lambda (x) (+ x x)) 5)" | java Parser
	-echo "((lambda (H) ((lambda (f) ((lambda (d/dx) (d/dx 10)) (lambda (x) (/ (- (f (+ x H)) (f x) H))))) (lambda (x) (* x x x x)))) .001)" | java Parser
	@echo "===================================================================="
	@echo "Testing Let var ..."
	-echo "(let ((x 5)) (let ((double (lambda (x) (+ x x)))) (funcall double 10)))" | java Parser
	-echo "(let ((x 3)) (let ((f (lambda (y) (+ x y)))) (let  ((x 5)) (funcall f 4))))" | java Parser
	-echo "(let ((x (+ 5 5))) (+ x x))" | java Parser
	-echo "(let ((x 5))(+ x x))" | java Parser
	-echo "(let ((x (+ 5 5))) (let ((y (- x 3))) (+ y y)))" | java Parser
	-echo "(let ((x 5)) (let ((y (- x 3))) (+ y y)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((x 3)) 10)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((x 3)) x)))" | java Parser
	-echo "(let ((x 5)) (+ x (let ((y 3)) x)))" | java Parser
	-echo "(let ((x 5)) (let ((y x)) y))" | java Parser
	-echo "(let ((x 5)) (let ((x x)) x))" | java Parser
	-echo "(let ((H .001)) (let ((f (lambda (x) (* x x x x)))) (let ((d/dx (lambda (x) (/ (- (funcall f (+ x H)) (funcall f x)) H)))) (funcall d/dx 10))))" | java Parser
	@echo "===================================================================="
	@echo "Testing Let multi-var bindings ..."
	-echo "(let ((x 5) (y 10)) (+ x y))" | java Parser
	-echo "(let ((x 5) (y 10) (z 15)) (+ x y z))" | java Parser
	-echo "(let ((x 5) (y 10)) (let ((f (lambda (g) (* g (/ y x))))) (funcall f (+ x y))))" | java Parser
	@echo "===================================================================="
	@echo "Testing LetRec (foldr) ..."
	-echo "(letrec ((f (lambda (f1 v l) (if l (funcall f1 (car l) (funcall f f1 v (cdr l))) v)))) (funcall f cons () (1 2 3 4 5 6)))" | java Parser
	@echo "===================================================================="
	@echo "Testing LetRec (foldl) ..."
	-echo "(letrec ((f (lambda (f1 v l) (if l (funcall f f1 (funcall f1 (car l) v) (cdr l)) v)))) (funcall f cons () (1 2 3 4 5 6)))" | java Parser
	@echo "===================================================================="
	@echo "Testing Multiple Parameters ..."
	-echo "(let ((sum (lambda (x y) (+ x y)))) (funcall sum 5 10))" | java Parser
	@echo "===================================================================="

checkcomb : all
	reset
	@echo "Testing Combinators ..."
	-echo "(<i> 10)" | java Parser
	-echo "(<k> 10 20)" | java Parser
	-echo "(<s> (lambda (x y) (+ x y)) (lambda (y) (* y y)) 10)" | java Parser
	-echo "(<c> (lambda (x y) (- x y)) 10 20)" | java Parser
	-echo "(<b> (lambda (x) (* x x)) (lambda (x) (+ x x)) 10)" | java Parser
	@echo "===================================================================="

clean:
	rm -f Parser.java
	rm -f ParseException.java
	rm -f ParserConstants.java
	rm -f ParserTokenManager.java
	rm -f Token.java
	rm -f TokenMgrError.java
	rm -f SimpleCharStream.java
	find ./ -name "*.class" | xargs rm -f $1
