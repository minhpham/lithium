(in-package :std-deck)

(defvar **suits** '(hearts diamond club spades))
(defvar **pips** '(ace two three four five six seven eight nine ten jack queen king))

;;; Return the suit of a card
(defun suit (card)
  (second card))

;;; Return the pip of a card
(defun pip (card)
  (first card))

;;; Return non-nil if a list is a card
(defun card? (list)
  (and (member (suit list) **suits**)
       (member (pip list) **pips**)))

;;; Make a card
(defun make-card (pip suit)
  (list pip suit))

;;; Return nil if the suit of card is not red
(defun red? (card)
  (member (suit card) '(heart diamond)))

;;; Return nil if the suit of card is not black
(defun black? (card)
  (member (suit card) '(club spades)))

;;; Return all possible cards in a deck
(defun make-deck ()
  (let ((deck '()))
    (dolist (pip **pips**)
      (dolist (suit **suits**)
        (push (make-card pip suit) deck)))
    deck))

;;; Cut the deck in at the depth given and put the top to the bottom
;;; If the depth is larger than the size of the deck, it will be
;;; reduced by the size of the deck until it is less.
;;; A depth of zero does not modify the deck
(defun cut-deck (deck depth)
  (setf depth (mod depth (length deck)))

  (let ((top (remove-if 'card? deck
                        :count (- (length deck) depth) :from-end t))
        (bottom (remove-if 'card? deck :count depth)))
    (append bottom top)))

;;; Helper shuffle function
(defun shuffle-rec (deck pos count)
  (if (<= count 0)
      deck
      (shuffle-rec (cut-deck deck (+ pos (random (/ pos 2)))) pos
                   (- 1 count))))

;;; Shuffle the given deck
(defun shuffle (deck)
  (shuffle-rec deck (/ (length deck) 2) 10))