package lithium.visitor;

import lithium.ast.*;
import lithium.Cons;

public class InterpretVisitor extends Visitor {
    public InterpretVisitor() {}

    public EnvNode cyclicBindInterp(ListNode args,
            ListNode vals, EnvNode... dsub) {
        Cons lst;
        Node n;

        // Construct a new value holder list
        ListNode valueHolder = new ListNode();
        lst = args.values;
        while (lst != null) {
            valueHolder.values = Cons.push(valueHolder.values,
                                           new SymbolNode("whatever"));
            lst = Cons.cdr(lst);
        }

        // Construct a new environment using the value holder list
        RecEnvNode newEnv = new RecEnvNode(args, valueHolder, dsub[0]);

        // Interpret the list of arguments using the new environment
        ListNode newVals = new ListNode();
        lst = vals.values;
        while (lst != null) {
            n = ((Node)Cons.car(lst)).accept(this, newEnv);
            newVals.values = Cons.push(newVals.values, n);
            lst = lst.cdr();
        }

        // Replace the value holders with the interpreted arguments
        EnvNode tmp = newEnv;
        lst = newVals.values;
        while (tmp != null && lst != null) {
            tmp.binding.setcdr(Cons.list(Cons.car(lst)));
            lst = lst.cdr();
            tmp = tmp.dsubs;
        }
        return newEnv;
    }

    @Override
    public Node visit(Node node, EnvNode... dsub) {
        return null;
    }

    @Override
    public Node visit(NumNode node, EnvNode... dsub) {
        return node;
    }

    @Override
    public Node visit(IdNode node, EnvNode... dsub) {
        Node ret = EnvNode.lookup(dsub[0], node.symbol);
        assert(ret != null);
        return ret;
    }

    @Override
    public Node visit(SymbolNode node, EnvNode... dsub) {
        return node;
    }

	@Override
    public Node visit(ListNode node, EnvNode... dsub) {
		return node;
    }
    
	@Override
    public Node visit(AddNode node, EnvNode... dsub) {
        NumNode sum = new NumNode(new Float(0));
        Cons nodes = node.values;
        while (nodes != null) {
            NumNode n = (NumNode)((Node)nodes.car()).accept(this, dsub);
            sum.value = sum.value + n.value;
            nodes = nodes.cdr();
        }
        return sum;
    }

	@Override
    public Node visit(SubNode node, EnvNode... dsub) {
        Cons nodes = node.values;
        NumNode sum = (NumNode)((Node)nodes.car()).accept(this, dsub);
        nodes = nodes.cdr();
        while (nodes != null) {
            NumNode n = (NumNode)((Node)nodes.car()).accept(this, dsub);
            sum.value = sum.value - n.value;
            nodes = nodes.cdr();
        }
        return sum;
    }

	@Override
    public Node visit(MulNode node, EnvNode... dsub) {
        NumNode fact = new NumNode(new Float(1));
        Cons nodes = node.values;
        while (nodes != null) {
            NumNode n = (NumNode)((Node)nodes.car()).accept(this, dsub);
            fact.value = fact.value * n.value;
            nodes = nodes.cdr();
        }
        return fact;
    }

    @Override
    public Node visit(DivNode node, EnvNode... dsub) {
        Cons nodes = node.values;
        NumNode fact = (NumNode)((Node)nodes.car()).accept(this, dsub);
        nodes = nodes.cdr();
        while (nodes != null) {
            NumNode n = (NumNode)((Node)nodes.car()).accept(this, dsub);
            fact.value = fact.value / n.value;
            nodes = nodes.cdr();
        }
        return fact;
    }

    @Override
    public Node visit(CarNode node, EnvNode... dsub) {
        Cons lst = ((ListNode)node.lst.accept(this, dsub)).values;
        return (Node)Cons.car(lst);
    }

    @Override
    public Node visit(CdrNode node, EnvNode... dsub) {
        Cons lst = ((ListNode)node.lst.accept(this, dsub)).values;
        return new ListNode(Cons.cdr(lst));
    }

    @Override
    public Node visit(ConsNode node, EnvNode... dsub) {
        Node atm = node.atom.accept(this, dsub);
        Cons lst = ((ListNode)node.list.accept(this, dsub)).values;
        return new ListNode(Cons.cons(atm, lst));
    }

    @Override
    public Node visit(LambdaNode node, EnvNode... dsub) {
        return new ClosureNode(node, dsub[0]);
    }

    @Override
    public Node visit(ClosureNode node, EnvNode... dsub) {
        return node;
    }

    @Override
    public Node visit(ApplyNode node, EnvNode... dsub) {
        ClosureNode funcVal;
        Node fv = node.func.accept(this, dsub);
        while (!(fv instanceof ClosureNode))
            fv = fv.accept(this, dsub);
        funcVal = (ClosureNode) fv;

        // Interpret the list of arguments
        ListNode initArgs = new ListNode();
        Cons lst = node.args.values;
        while (lst != null) {
            Node tmp = ((Node)Cons.car(lst)).accept(this, dsub);
            initArgs.values = Cons.push(initArgs.values, tmp);
            lst = lst.cdr();
        }

        // Construct new environment
        EnvNode newEnv = new EnvNode(funcVal.func.args, initArgs);
        newEnv = EnvNode.combine(newEnv, funcVal.dsub);

        // Interpret function body using new environment
        return funcVal.func.body.accept(this, newEnv);
    }

    @Override
    public Node visit(EnvNode node, EnvNode... dsub) {
        return node;
    }

    boolean isNull(Node n) {
        if (n instanceof ListNode) {
            return Cons.nullp(((ListNode) n).values);
        }
        else {
            return false;
        }
    }

    boolean isTrue(Node n) {
        return !isNull(n);
    }

    @Override
    public Node visit(IfNode node, EnvNode... dsub) {
        if (isNull(node.test.accept(this, dsub))) {
            return node.falseBranch.accept(this, dsub);
        }
        else {
            return node.trueBranch.accept(this, dsub);
        }
    }

    @Override
    public Node visit(RecApplyNode node, EnvNode... dsub) {
        ClosureNode funcVal;
        Node fv = node.func.accept(this, dsub);
        while (!(fv instanceof ClosureNode))
            fv = fv.accept(this, dsub);
        funcVal = (ClosureNode) fv;
        return funcVal.func.body.accept(this, this.cyclicBindInterp(funcVal.func.args, node.args, dsub));
    }
}
