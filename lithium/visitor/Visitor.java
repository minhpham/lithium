package lithium.visitor;

import lithium.ast.*;

public abstract class Visitor {
    abstract public Node visit(Node node, EnvNode... dsub);

    abstract public Node visit(NumNode node, EnvNode... dsub);
    abstract public Node visit(IdNode node, EnvNode... dsub);
    abstract public Node visit(SymbolNode node, EnvNode... dsub);

    abstract public Node visit(ListNode node, EnvNode... dsub);
    abstract public Node visit(AddNode node, EnvNode... dsub);
    abstract public Node visit(SubNode node, EnvNode... dsub);
    abstract public Node visit(MulNode node, EnvNode... dsub);
    abstract public Node visit(DivNode node, EnvNode... dsub);
    
    abstract public Node visit(CarNode node, EnvNode... dsub);
    abstract public Node visit(CdrNode node, EnvNode... dsub);
    abstract public Node visit(ConsNode node, EnvNode... dsub);

    abstract public Node visit(LambdaNode node, EnvNode... dsub);
    abstract public Node visit(ClosureNode node, EnvNode... dsub);
    abstract public Node visit(ApplyNode node, EnvNode... dsub);
    abstract public Node visit(EnvNode node, EnvNode... dsub);

    abstract public Node visit(IfNode node, EnvNode... dsub);
    
    abstract public Node visit(RecApplyNode node, EnvNode... dsub);
}
