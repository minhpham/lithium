package lithium.visitor;

import lithium.ast.*;
import lithium.Cons;

public class PrintVisitor extends Visitor {
    public PrintVisitor() {}

    @Override
    public Node visit(Node node, EnvNode... dsub) {
        node.printValue = Cons.cons(null, null);
        System.out.print(node.printValue.toString());
        return null;
    }

    @Override
    public Node visit(NumNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(IdNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(SymbolNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(ListNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(AddNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(SubNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(MulNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(DivNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(CarNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }

    @Override
    public Node visit(CdrNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }

    @Override
    public Node visit(ConsNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }
    @Override
    public Node visit(LambdaNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(ClosureNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(ApplyNode node, EnvNode... dsub) {
        System.out.print(node.toString());
        return null;
    }

    @Override
    public Node visit(EnvNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }

    @Override
    public Node visit(IfNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }

    @Override
    public Node visit(RecApplyNode node, EnvNode... dsub) {
        System.out.print(node);
        return null;
    }
}
