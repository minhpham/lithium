package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class MulNode extends ListNode {

    public Cons values;

    public MulNode() {
        super();
        this.values = null;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new SymbolNode("mul"),
                                    this.values);
        return this.printValue.toString();
    }

}
