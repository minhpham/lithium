package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class AddNode extends ListNode {

    public Cons values;

    public AddNode() {
        super();
        this.values = null;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new SymbolNode("add"),
                                    this.values);
        return this.printValue.toString();
    }

}
