package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class CdrNode extends ListNode {
    public Node lst;
    
    public CdrNode(Node lst) {
        this.lst = lst;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("cdr"),
                this.lst);
        return printValue.toString();
    }

}