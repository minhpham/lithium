package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class ConsNode extends ListNode {
    public Node atom;
    public Node list;
    
    public ConsNode(Node n, Node lst) {
        this.atom = n;
        this.list = lst;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("cons"),
                this.atom,
                this.list);
        return printValue.toString();
    }

}
