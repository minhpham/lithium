package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class IdNode extends Node {
    public String symbol;

    public IdNode(String symbol) {
        super();
        this.symbol = symbol;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new String("id"),
                                    Cons.list(this.symbol));
        return this.printValue.toString();
    }

}
