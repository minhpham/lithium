package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class IfNode extends Node {
    public Node test;
    public Node trueBranch;
    public Node falseBranch;

    public IfNode() {
        this.test = null;
        this.trueBranch = null;
        this.falseBranch = null;
    }

    public IfNode(Node test, Node trueBranch, Node falseBranch) {
        this.test = test;
        this.trueBranch = trueBranch;
        this.falseBranch = falseBranch;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("if"),
                this.test,
                this.trueBranch,
                this.falseBranch);
        return this.printValue.toString();
    }

}
