package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class LambdaNode extends Node {
	public ListNode args;
	public Node body;

	public LambdaNode() {
		super();
		this.args = null;
		this.body = null;
	}
	
	public LambdaNode(ListNode args, Node body) {
		super();
		this.args = args;
		this.body = body;
	}
	
    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }
    
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("lambda"),
                        this.args,
                        this.body);
    	return this.printValue.toString();
    }

}
