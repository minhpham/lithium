package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class ListNode extends Node {
    public Cons values;

    public ListNode() {
        super();
        this.values = null;
    }

    public ListNode(Cons values) {
        super();
        this.values = values;
    }

    public ListNode(Node... nodes) {
        super();
        if (nodes.length > 0) {
            for (int i = 0; i < nodes.length; i++) {
                this.values = Cons.push(this.values, nodes[i]);
            }
        }
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        return Cons.toString(this.values);
    }
}
