package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class NumNode extends Node {
    public Double value;

    public NumNode(Float value) {
        super();
        this.value = new Double(value);
    }

    public NumNode(int value) {
        super();
        this.value = new Double(value);
    }

    public NumNode(double value) {
        super();
        this.value = new Double(value);
    }


    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new String("num"),
                                    Cons.list(this.value));
        return this.printValue.toString();
    }
}

