package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class SubNode extends ListNode {

    public Cons values;

    public SubNode() {
        super();
        this.values = null;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new SymbolNode("sub"),
                                    this.values);
        return this.printValue.toString();
    }

}
