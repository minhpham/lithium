package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class DivNode extends ListNode {

    public Cons values;

    public DivNode() {
        super();
        this.values = null;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.cons(new SymbolNode("div"),
                                    this.values);
        return this.printValue.toString();
    }

}
