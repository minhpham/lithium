package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class RecApplyNode extends ApplyNode {
    public RecApplyNode() {
        super();
    }

    public RecApplyNode(Node func) {
        super(func);
    }

    public RecApplyNode(Node func, ListNode args) {
        super(func, args);
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("recapply"),
                                    this.func,
                                    this.args);
        return this.printValue.toString();
    }
}
