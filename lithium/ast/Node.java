package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public abstract class Node {
    public Cons printValue;

    Node() {
        this.printValue = null;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }
    
    abstract public String toString();
}
