package lithium.ast;

import lithium.Cons;

public class RecEnvNode extends EnvNode {

    public RecEnvNode() {
        // TODO Auto-generated constructor stub
        super();
    }

    public RecEnvNode(Cons binding) {
        super(binding);
        // TODO Auto-generated constructor stub
    }

    public RecEnvNode(Cons binding, EnvNode dsubs) {
        super(binding, dsubs);
        // TODO Auto-generated constructor stub
    }

    public RecEnvNode(ListNode args, ListNode vals) {
        super(args, vals);
        // TODO Auto-generated constructor stub
    }
    
    public RecEnvNode(ListNode args, ListNode vals, EnvNode dsubs) {
        super();
        Cons argList = args.values;
        Cons valList = vals.values;

        EnvNode temp = this;
        while (temp.dsubs != null)
            temp = temp.dsubs;

        while (argList != null && valList != null) {
            IdNode arg = (IdNode) Cons.car(argList);
            Node val = (Node) Cons.car(valList);

            temp.binding = Cons.list(arg, val);
            temp.dsubs = new EnvNode();
            temp = temp.dsubs;

            argList = Cons.cdr(argList);
            valList = Cons.cdr(valList);
        }

        if (dsubs != null) {
            temp.binding = dsubs.binding;
            temp.dsubs = dsubs.dsubs;
        }
    }
}
