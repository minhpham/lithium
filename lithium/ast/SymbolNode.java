package lithium.ast;

import lithium.visitor.Visitor;

public class SymbolNode extends Node {
    public String symbol;

    public SymbolNode(String symbol) {
        super();
        this.symbol = symbol;
    }

    public Node accept(Visitor visitor) {
        return visitor.visit(this);
    }

    public String toString() {
        return this.symbol;
    }
}
