package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class CarNode extends Node {
    public Node lst;

    public CarNode(Node lst) {
        this.lst = lst;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("car"),
                this.lst);
        return printValue.toString();
    }

}