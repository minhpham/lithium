package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class ClosureNode extends Node {
    public LambdaNode func;
    public EnvNode dsub;

    public ClosureNode() {
        super();
        this.func = null;
        this.dsub = new EnvNode();
    }

    public ClosureNode(LambdaNode func, EnvNode dsub) {
        super();
        this.func = func;
        this.dsub = dsub;
    }

    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public String toString() {
        this.printValue = Cons.list(new SymbolNode("closure"),
                                    this.func,
                                    this.dsub);
        return this.printValue.toString();
    }
}
