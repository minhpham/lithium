package lithium.ast;

import lithium.Cons;
import lithium.visitor.Visitor;

public class EnvNode extends Node {
    public Cons binding;
    public EnvNode dsubs;

    public EnvNode() {
        super();
        this.binding = null;
        this.dsubs = null;
    }
    
    public EnvNode(Cons binding) {
        super();
        this.binding = binding;
        this.dsubs = new EnvNode();
    }
    
    public EnvNode(Cons binding, EnvNode dsubs) {
        super();
        this.binding = binding;
        if (dsubs != null)
            this.dsubs = new EnvNode(dsubs.binding, dsubs.dsubs);
    }

    public EnvNode(ListNode args, ListNode vals) {
        super();
        Cons argList = args.values;
        Cons valList = vals.values;

        EnvNode temp = this;
        while (temp.dsubs != null)
            temp = temp.dsubs;

        while (argList != null && valList != null) {
            IdNode arg = (IdNode) Cons.car(argList);
            Node val = (Node) Cons.car(valList);

            temp.binding = Cons.list(arg, val);
            temp.dsubs = new EnvNode();
            temp = temp.dsubs;

            argList = Cons.cdr(argList);
            valList = Cons.cdr(valList);
        }
    }
    
    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }

    public static EnvNode combine(EnvNode env1, EnvNode env2) {
        EnvNode ret;
        if (env1 == null) {
            ret = env2;
        }
        else if (env2 == null) {
            ret = env1;
        }
        else {
            ret = new EnvNode(env1.binding, env1.dsubs);
            
            EnvNode tmp = ret;
            
            while (ret.dsubs != null) {
                ret = ret.dsubs;
            }
            ret.binding = env2.binding;
            ret.dsubs = env2.dsubs;
            
            ret = tmp;
        }
        return ret;
    }

    public static EnvNode bind(EnvNode env, IdNode symbol, Node value) {
        EnvNode newBinding = new EnvNode(Cons.list(symbol, value));
        return combine(env, newBinding);
    }

    public static Node lookup(EnvNode env, String symbol) {
        if (env == null)
            return null;
        else if (((IdNode)Cons.car(env.binding)).symbol.equals(symbol))
            return ((Node)Cons.car(Cons.cdr(env.binding)));
        else if (env.dsubs == null)
            return null;
        else
            return lookup(env.dsubs, symbol);
    }

    public String toString() {
        if (this.binding == null) {
            this.printValue = Cons.list(new SymbolNode("mtSub"));
        }
        else {
            this.printValue = Cons.list(new SymbolNode("aSub"),
                                        this.binding, this.dsubs);
        }
        return this.printValue.toString();
    }
}
