package lithium.ast;

import lithium.visitor.Visitor;
import lithium.Cons;

public class ApplyNode extends Node {
    public Node func;
    public ListNode args;

    public ApplyNode() {
        super();
        this.func = null;
        this.args = null;
    }
    
    public ApplyNode(Node func) {
        super();
        this.func = func;
        this.args = null;
    }

    public ApplyNode(Node func, ListNode args) {
        super();
        this.func = func;
        this.args = args;
    }
    
    @Override
    public Node accept(Visitor visitor, EnvNode... dsub) {
        return visitor.visit(this, dsub);
    }
    
    @Override
    public String toString() {
        this.printValue = Cons.list(new SymbolNode("apply"),
                                    this.func,
                                    this.args);
        return this.printValue.toString();
    }
}
