package lithium;

public class Cons {
    private Object car;
    private Cons cdr;

    public Cons(Object car_, Cons cdr_) {
        this.car = car_;
        this.cdr = cdr_;
    }

    public Object car() {
        return this.car;
    }

    public Cons cdr() {
        return this.cdr;
    }

    public Cons next() {
        return (this.cdr == null) ? this : this.cdr;
    }

    public static Object car(Cons lst) {
        return (lst == null) ? null : lst.car;
    }

    public static Cons cdr(Cons lst) {
        return (lst == null) ? null : lst.cdr;
    }

    public static Cons cons(Object first, Cons rest) {
        return new Cons(first, rest);
    }

    public static boolean nullp(Cons lst) {
        return ((lst == null) || (lst.car() == null && lst.cdr() == null));
    }

    public void setcar(Object obj) {
        this.car = obj;
    }

    public void setcdr(Cons cns) {
        this.cdr = cns;
    }

    public static Cons push(Cons lst, Object obj) {
        if (lst == null) {
            return list(obj);
        }
        else {
            Cons iter = lst;
            while (cdr(iter) != null) {
                iter = cdr(iter);
            }
            iter.cdr = list(obj);
            return lst;
        }
    }

    public static boolean consp (Object o) {
        return ( (o != null) && (o instanceof Cons) );
    }

    public static Cons list(Object ... elems) {
       Cons list = null;
       for (int i = elems.length-1; i >= 0; i--) {
           list = cons(elems[i], list);
       }
       return list;
   }

    public String toString() {
        return toString(this);
    }

    public static String toString(Cons lst) {
        return "(" + toStringInternal(lst);
    }

    public static String toStringInternal(Cons lst) {
        return ((lst == null) ? ")"
                                : ((car(lst) == null) ? "()" : car(lst).toString())
                                + ((cdr(lst) == null) ? ")" : " " + toStringInternal(cdr(lst))));
   }
}
