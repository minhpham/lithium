;;;; Loader file and facade for card games

(defpackage :std-deck
  (:use :cl :cl-user)
  (:export :suit
           :pip
           :make-card
           :red?
           :black?
           :make-deck
           :shuffle))

(defpackage :freecell
  (:use :cl :cl-user :std-deck))

(load "std-deck")
(load "freecell")